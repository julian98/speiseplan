"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LaunchRequestHandler = /** @class */ (function () {
    function LaunchRequestHandler() {
    }
    LaunchRequestHandler.prototype.canHandle = function (input) {
        return input.requestEnvelope.request.type === "LaunchRequest";
    };
    LaunchRequestHandler.prototype.handle = function (input) {
        return input.responseBuilder
            .speak("Ich bin mächtig.")
            .reprompt("Bitte sprich zu mir!")
            .getResponse();
    };
    return LaunchRequestHandler;
}());
exports.LaunchRequestHandler = LaunchRequestHandler;
//# sourceMappingURL=LaunchRequestHandler.js.map