"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var ask_sdk_express_adapter_1 = require("ask-sdk-express-adapter");
var ask_sdk_core_1 = require("ask-sdk-core");
var LaunchRequestHandler_1 = require("./LaunchRequestHandler");
var https_1 = require("https");
var fs_1 = require("fs");
var app = express();
var skillBuilder = ask_sdk_core_1.SkillBuilders.custom()
    .addRequestHandlers(new LaunchRequestHandler_1.LaunchRequestHandler()).withSkillId("amzn1.ask.skill.dc69ecfa-303c-4a66-8c2a-afc0cf0e5be5");
var skill = skillBuilder.create();
var adapter = new ask_sdk_express_adapter_1.ExpressAdapter(skill, true, true);
app.post('/', adapter.getRequestHandlers());
var options = {
    key: fs_1.readFileSync("cert.key"),
    cert: fs_1.readFileSync("cert.cert")
};
var server = https_1.createServer(options, app);
server.listen(443, function () { return console.log("Server started."); });
//# sourceMappingURL=index.js.map