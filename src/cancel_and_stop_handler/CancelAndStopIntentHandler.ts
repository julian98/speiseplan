import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class CancelAndStopIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === "IntentRequest"
            && (request.intent.name === 'AMAZON.CancelIntent'
                || request.intent.name === 'AMAZON.StopIntent');
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .speak(`${Messages.lastSentence[Math.floor(Math.random() * Messages.lastSentence.length)]}`)
            .withShouldEndSession(true)
            .getResponse();
    }

}