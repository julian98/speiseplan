import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Messages} from "../utils/Messages";
import {database} from "../index";
import {CustomDateFormatter} from "../utils/CustomDateFormatter";
import {Response} from "ask-sdk-model";

export class CompletedChangeIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ChangeIntent'
            && request.dialogState === 'COMPLETED';
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const dayInput = request.intent.slots.dayOfWeek.value;
        const outputRealDay = CustomDateFormatter.getRealDate(dayInput.toLowerCase());
        if (outputRealDay === 'Samstag' || outputRealDay === 'Sonntag') {
            const text = `${dayInput} ist kein Arbeitstag im Unternehmen. Dementsprechend ist die Kantine ${dayInput} geschlossen. ` +
                `Bitte nenne mir einen Wochentag zwischen Montag und Freitag.`;
            return input.responseBuilder
                .speak(text)
                .reprompt(text)
                .addElicitSlotDirective('dayOfWeek')
                .getResponse();
        }

        const menuTypeInput = request.intent.slots.menuType.resolutions.resolutionsPerAuthority[0].values[0].value.name;
        const menuInput = Messages.capitalize(request.intent.slots.menu.value);

        try {
            await database.query('UPDATE tagesplan SET ' + menuTypeInput.toLowerCase() + ' = ' + '"' +
                menuInput + '"' + ' WHERE tag_name = ' + '"' + outputRealDay + '"');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }

        const speechText = `Alles klar. ` +
            `Ich habe die ${menuTypeInput} für ${dayInput} zu ${menuInput} geändert. ` +
            `${Messages.lastSentence[Math.floor(Math.random() * Messages.lastSentence.length)]}`;
        return input.responseBuilder
            .speak(speechText)
            .withShouldEndSession(true)
            .getResponse()
    }

}
