import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {database} from "../index";
import {CustomDateFormatter} from "../utils/CustomDateFormatter";

export class DayOfWeekAndMenuTypeChangeIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ChangeIntent'
            && request.intent.slots.dayOfWeek.value
            && request.intent.slots.menuType.value
            && !request.intent.slots.menu.value;
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const dayInput = request.intent.slots.dayOfWeek.value;
        const outputRealDay = CustomDateFormatter.getRealDate(dayInput.toLowerCase());
        if (outputRealDay === 'Samstag' || outputRealDay === 'Sonntag') {
            const text = `${dayInput} ist kein Arbeitstag im Unternehmen. Dementsprechend ist die Kantine ${dayInput} geschlossen. ` +
                `Bitte nenne mir einen Wochentag zwischen Montag und Freitag.`;
            return input.responseBuilder
                .speak(text)
                .reprompt(text)
                .addElicitSlotDirective('dayOfWeek')
                .getResponse();
        }

        const menuTypeInput = request.intent.slots.menuType.resolutions.resolutionsPerAuthority[0].values[0].value.name;

        let result;
        try {
            result = await database.query('SELECT ' + menuTypeInput.toLowerCase() +
                ' FROM tagesplan WHERE tag_name = ' + '"' + outputRealDay + '"');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }
        const oldMenuJson = JSON.parse(JSON.stringify(result))[0];
        const oldMenu = oldMenuJson[Object.keys(oldMenuJson)[0]];

        const speechText = `Sehr gerne. ` +
            `Aktuell ist für ${outputRealDay} als ${menuTypeInput} ${oldMenu} eingeplant. ` +
            `Welches Gericht soll es in der Kantine ${dayInput} stattdessen zu Essen geben?`;
        const repromtText = `Ich benötige die Angabe, welches Gericht ${dayInput} die neue ${menuTypeInput} werden soll.`;
        return input.responseBuilder
            .speak(speechText)
            .reprompt(repromtText)
            .addElicitSlotDirective('menu')
            .getResponse();
    }

}