import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";
import {CustomDateFormatter} from "../utils/CustomDateFormatter";

export class DayOfWeekChangeIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ChangeIntent'
            && request.intent.slots.dayOfWeek.value
            && !request.intent.slots.menuType.value
            && !request.intent.slots.menu.value;
    }

    handle(input: HandlerInput): Response {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const dayInput = request.intent.slots.dayOfWeek.value;
        const outputRealDay = CustomDateFormatter.getRealDate(dayInput.toLowerCase());
        if (outputRealDay === 'Samstag' || outputRealDay === 'Sonntag') {
            const text = `${dayInput} ist kein Arbeitstag im Unternehmen. Dementsprechend ist die Kantine ${dayInput} geschlossen. ` +
                `Bitte nenne mir einen Wochentag zwischen Montag und Freitag.`;
            return input.responseBuilder
                .speak(text)
                .reprompt(text)
                .addElicitSlotDirective('dayOfWeek')
                .getResponse();
        }

        const speechText = `${Messages.firstSentence[Math.floor(Math.random() * Messages.firstSentence.length)]} ` +
            `Möchtest du das Hauptgericht, die Nachspeise oder die Suppe für ${dayInput} ändern?`;
        const repromtText = `Ich benötige die Angabe des Gerichtstypen, den du am ${outputRealDay} ändern möchtest. ` +
            `Den Gästen der Kantine stehen täglich Hauptgericht, Nachspeise oder die Tagessuppe zur Aushwal.`;
        return input.responseBuilder
            .speak(speechText)
            .reprompt(repromtText)
            .addElicitSlotDirective('menuType')
            .getResponse();
    }

}

