import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class MenuTypeAndMenuChangeIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ChangeIntent'
            && request.intent.slots.menuType.value
            && request.intent.slots.menu.value
            && !request.intent.slots.dayOfWeek.value;
    }

    handle(input: HandlerInput): Response {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const menuTypeInput = request.intent.slots.menuType.resolutions.resolutionsPerAuthority[0].values[0].value.name;
        const menuInput = Messages.capitalize(request.intent.slots.menu.value);

        return input.responseBuilder
            .speak(`Okay. Für welchen Tag möchtest du die ${menuTypeInput} zu ${menuInput} ändern?`)
            .reprompt('Ich benötige die Angabe des Wochentages, an dem du ein Gericht ändern möchtest.')
            .addElicitSlotDirective('dayOfWeek')
            .getResponse();
    }

}