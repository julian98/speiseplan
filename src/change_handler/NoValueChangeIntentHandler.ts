import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class NoValueChangeIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'ChangeIntent'
            && !request.intent.slots.dayOfWeek.value
            && !request.intent.slots.menuType.value
            && !request.intent.slots.menu.value;
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .speak(`${Messages.firstSentence[Math.floor(Math.random() * Messages.firstSentence.length)]} ` +
                `Für welchen Tag möchtest du das Gericht ändern?`)
            .reprompt('Ich benötige die Angabe des Wochentages, an dem du ein Gericht ändern möchtest.')
            .addElicitSlotDirective('dayOfWeek')
            .getResponse();
    }

}