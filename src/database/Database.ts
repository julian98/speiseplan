import {createPool, Pool} from "mysql";
import {readFileSync} from "fs";

export class Database {

    private readonly pool: Pool;

    constructor() {
        const rawFile = readFileSync('database.json', {encoding: 'utf8'});
        const creds = JSON.parse(rawFile);

        this.pool = createPool({
            host: creds.host,
            user: creds.user,
            password: creds.password,
            database: creds.database
        });
    }

    query(query: string) {
        return new Promise((resolve, reject) => {
            this.pool.query(query, (err, results) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(results);
            });
        }).catch(reason => {
            console.error(reason);
        });
    }

}