import {ErrorHandler, HandlerInput} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class CustomErrorHandler implements ErrorHandler {

    canHandle(input: HandlerInput, error: Error): boolean {
        return true;
    }

    handle(input: HandlerInput, error: Error): Response {
        console.log(`Error handled: ${error.message}`);

        return input.responseBuilder
            .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler. Was sagtest du?')
            .reprompt('Kann ich dir noch weiterhelfen?')
            .getResponse();
    }

}