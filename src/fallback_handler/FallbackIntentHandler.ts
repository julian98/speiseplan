import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class FallbackIntentHandler implements RequestHandler {

    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'AMAZON.FallbackIntent';
    }

    handle(input: HandlerInput): Response {
        const text = `${Messages.unknownSentence[Math.floor(Math.random() * Messages.unknownSentence.length)]} ` +
            `${Messages.helpSentence[Math.floor(Math.random() * Messages.helpSentence.length)]}`;
        return input.responseBuilder
            .speak(text)
            .reprompt(text)
            .getResponse();
    }

}