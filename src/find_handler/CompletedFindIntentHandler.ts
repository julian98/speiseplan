import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {database} from "../index";

export class CompletedFindIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'FindIntent'
            && request.dialogState === 'COMPLETED';
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const menu = request.intent.slots.menu.value;

        let result;
        try {
            result = await database.query('SELECT * FROM tagesplan');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }

        const weekJson = JSON.parse(JSON.stringify(result));

        for (let i = 0; i < weekJson.length; i++) {
            const currentDay = weekJson[i];
            if (menu.toLowerCase() === (currentDay['hauptspeise'] as string).toLowerCase()) {
                return input.responseBuilder
                    .speak(`Das Gericht ${menu} wird ${currentDay['tag_name']} in der Kantine als Hauptspeise angeboten. ` +
                        `Lass es dir schmecken!`)
                    .withShouldEndSession(true)
                    .getResponse();
            } else if (menu.toLowerCase() === (currentDay['nachspeise'] as string).toLowerCase()) {
                return input.responseBuilder
                    .speak(`Das Gericht ${menu} wird ${currentDay['tag_name']} in der Kantine als Nachspeise angeboten. ` +
                        `Guten Appetit!`)
                    .withShouldEndSession(true)
                    .getResponse();
            } else if (menu.toLowerCase() === (currentDay['tagessuppe'] as string).toLowerCase()) {
                return input.responseBuilder
                    .speak(`${menu} wird ${currentDay['tag_name']} in der Kantine als Tagessuppe angeboten. ` +
                        `Vorsicht, heiß!`)
                    .withShouldEndSession(true)
                    .getResponse();
            }
        }

        return input.responseBuilder
            .speak(`Das Gericht ${menu} ist im aktuellen Speiseplan leider nicht eingeplant. Versuch es doch nächste Woche nochmal!`)
            .withShouldEndSession(true)
            .getResponse();
    }

}
