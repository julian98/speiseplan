import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class NoValueFindIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'FindIntent'
            && !request.intent.slots.menu.value
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .speak(`${Messages.firstSentence[Math.floor(Math.random() * Messages.firstSentence.length)]} ` +
                `Nach welchem Gericht möchtest du den Speiseplan durchsuchen?`)
            .reprompt('Ich benötige die Angabe des Gerichtes, nach dem ich für dich suchen soll.')
            .addElicitSlotDirective('menu')
            .getResponse();
    }

}