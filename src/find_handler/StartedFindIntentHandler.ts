import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class StartedFindIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'FindIntent'
            && request.dialogState !== 'COMPLETED';
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .addDelegateDirective()
            .getResponse();
    }

}