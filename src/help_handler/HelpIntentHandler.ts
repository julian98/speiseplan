import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class HelpIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === "IntentRequest"
            && request.intent.name === 'AMAZON.HelpIntent';
    }

    handle(input: HandlerInput): Response {
        const text = 'Ich biete dir fünf verschiedene Funktionen. Du kannst Änderungen am Speiseplan vornehmen, dir eine Wochenübersicht ' +
            'generieren lassen oder ein einzelnes Menü im aktuellen Speiseplan suchen. Darüber hinaus kannst du dir konkrete ' +
            'Informationen über ein einzelnes Gericht an einem spezifischen Tag ausgeben lassen oder eine Tagesübersicht anfordern';
        return input.responseBuilder
            .speak(text)
            .withShouldEndSession(true)
            .getResponse();
    }

}