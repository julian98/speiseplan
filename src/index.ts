import * as express from 'express';
import {ExpressAdapter} from 'ask-sdk-express-adapter';
import {SkillBuilders} from "ask-sdk-core";
import {LaunchRequestHandler} from "./launch_handler/LaunchRequestHandler";
import {createServer, ServerOptions} from "https";
import {readFileSync} from "fs";
import {NoValueChangeIntentHandler} from "./change_handler/NoValueChangeIntentHandler";
import {StartedChangeIntentHandler} from "./change_handler/StartedChangeIntentHandler";
import {DayOfWeekChangeIntentHandler} from "./change_handler/DayOfWeekChangeIntentHandler";
import {DayOfWeekAndMenuTypeChangeIntentHandler} from "./change_handler/DayOfWeekAndMenuTypeChangeIntentHandler";
import {CompletedChangeIntentHandler} from "./change_handler/CompletedChangeIntentHandler";
import {CustomErrorHandler} from "./error_handler/CustomErrorHandler";
import {HelpIntentHandler} from "./help_handler/HelpIntentHandler";
import {CancelAndStopIntentHandler} from "./cancel_and_stop_handler/CancelAndStopIntentHandler";
import {SessionEndedHandler} from "./session_ended_handler/SessionEndedHandler";
import {FallbackIntentHandler} from "./fallback_handler/FallbackIntentHandler";
import {Messages} from "./utils/Messages";
import {Database} from "./database/Database";
import {NoValueRequestIntentHandler} from "./request_handler/NoValueRequestIntentHandler";
import {StartedRequestIntentHandler} from "./request_handler/StartedRequestIntentHandler";
import {DayOfWeekRequestIntentHandler} from "./request_handler/DayOfWeekRequestIntentHandler";
import {CompletedRequestIntentHandler} from "./request_handler/CompletedRequestIntentHandler";
import {CompletedFindIntentHandler} from "./find_handler/CompletedFindIntentHandler";
import {RequestAllIntentHandler} from "./request_all_handler/RequestAllIntentHandler";
import {CompletedRequestDayIntentHandler} from "./request_day_handler/CompletedRequestDayIntentHandler";
import {MenuTypeChangeIntentHandler} from "./change_handler/MenuTypeChangeIntentHandler";
import {MenuTypeRequestIntentHandler} from "./request_handler/MenuTypeRequestIntentHandler";
import {DayOfWeekAndMenuChangeIntentHandler} from "./change_handler/DayOfWeekAndMenuChangeIntentHandler";
import {MenuTypeAndMenuChangeIntentHandler} from "./change_handler/MenuTypeAndMenuChangeIntentHandler";
import {MenuChangeIntentHandler} from "./change_handler/MenuChangeIntentHandler";
import {StartedRequestDayIntentHandler} from "./request_day_handler/StartedRequestDayIntentHandler";
import {NoValueRequestDayIntentHandler} from "./request_day_handler/NoValueRequestDayIntentHandler";
import {StartedFindIntentHandler} from "./find_handler/StartedFindIntentHandler";
import {NoValueFindIntentHandler} from "./find_handler/NoValueFindIntentHandler";

export const database = new Database();
Messages.initialize();

const skill = SkillBuilders
    .custom()
    .addRequestHandlers(
        new LaunchRequestHandler(),

        new StartedChangeIntentHandler(),
        new NoValueChangeIntentHandler(),
        new DayOfWeekChangeIntentHandler(),
        new DayOfWeekAndMenuTypeChangeIntentHandler(),
        new DayOfWeekAndMenuChangeIntentHandler(),
        new MenuTypeChangeIntentHandler(),
        new MenuTypeAndMenuChangeIntentHandler(),
        new MenuChangeIntentHandler(),
        new CompletedChangeIntentHandler(),

        new StartedRequestIntentHandler(),
        new NoValueRequestIntentHandler(),
        new DayOfWeekRequestIntentHandler(),
        new MenuTypeRequestIntentHandler(),
        new CompletedRequestIntentHandler(),

        new StartedFindIntentHandler(),
        new NoValueFindIntentHandler(),
        new CompletedFindIntentHandler(),

        new RequestAllIntentHandler(),

        new StartedRequestDayIntentHandler(),
        new NoValueRequestDayIntentHandler(),
        new CompletedRequestDayIntentHandler(),

        new HelpIntentHandler(),
        new FallbackIntentHandler(),
        new SessionEndedHandler(),
        new CancelAndStopIntentHandler(),
    ).addErrorHandlers(
        new CustomErrorHandler()
    ).withSkillId("amzn1.ask.skill.c7a4adbf-4fdc-4a47-b642-091a00da8a47")
    .create();

const adapter = new ExpressAdapter(skill, true, true);
const app = express();
app.post('/', adapter.getRequestHandlers());

const options: ServerOptions = {
    key: readFileSync("cert.key"),
    cert: readFileSync("cert.cert")
};
const server = createServer(options, app);
server.listen(443, () =>
    console.log("Server started.")
);
