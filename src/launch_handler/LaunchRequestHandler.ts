import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";

export class LaunchRequestHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        return input.requestEnvelope.request.type === "LaunchRequest";
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .speak('Herzlich Willkommen beim Speiseplan-Skill. ' +
                `${Messages.helpSentence[Math.floor(Math.random() * Messages.helpSentence.length)]}`
            ).reprompt('Mit dem Speiseplan-Skill kannst du Änderungen am Speiseplan vornehmen, ' +
                'dir gezielt Auskünfte über einzelne Tage einholen oder eine Übersicht über alle Speisen ' +
                'der kommenden Arbeitswoche in Erfahrung bringen. ' +
                `${Messages.helpSentence[Math.floor(Math.random() * Messages.helpSentence.length)]}`)
            .getResponse();
    }

}