import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {database} from "../index";
import {Messages} from "../utils/Messages";

export class RequestAllIntentHandler implements RequestHandler {

    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestAllIntent';
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        let result;
        try {
            result = await database.query('SELECT * FROM tagesplan');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }

        const weekJson = JSON.parse(JSON.stringify(result));

        let outputText = "";
        for (let i = 0; i < weekJson.length; i++) {
            const currentDay = weekJson[i];
            outputText += this.fancyTextOfDay(currentDay);
        }

        return input.responseBuilder
            .speak(`${Messages.firstSentence[Math.floor(Math.random() * Messages.firstSentence.length)]} ` +
                `${outputText} ${Messages.lastSentence[Math.floor(Math.random() * Messages.lastSentence.length)]}`)
            .withShouldEndSession(true)
            .getResponse();

    }

    fancyTextOfDay(currentDay): string {
        const rdm = Math.floor(Math.random() * 6);
        if (rdm === 0) {
            return `Am ${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']}, als Nachspeise ${currentDay['nachspeise']} und ` +
                `als Tagessuppe ${currentDay['tagessuppe']}. `
        } else if (rdm === 1) {
            return `${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']}, als Nachspeise ${currentDay['nachspeise']} und ` +
                `als Tagessuppe ${currentDay['tagessuppe']}. `
        } else if (rdm === 2) {
            return `Am ${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']}, als Nachspeise ${currentDay['nachspeise']} und ` +
                `als Tagessuppe ${currentDay['tagessuppe']} zu essen. `
        } else if (rdm === 3) {
            return `${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']}, als Nachspeise ${currentDay['nachspeise']} und ` +
                `als Tagessuppe ${currentDay['tagessuppe']} zu essen. `
        } else if (rdm === 4) {
            return `Am ${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']} und ${currentDay['nachspeise']} als Nachspeise. ` +
                `Als Tagessuppe wird ${currentDay['tagessuppe']} angeboten. `
        } else if (rdm === 5) {
            return `${currentDay['tag_name']} gibt es als Hauptgericht ${currentDay['hauptspeise']} und als Nachspeise ${currentDay['nachspeise']}. ` +
                `${currentDay['tagessuppe']} wird als Tagessuppe angeboten. `
        }
    }

}