import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";
import {Messages} from "../utils/Messages";
import {CustomDateFormatter} from "../utils/CustomDateFormatter";
import {database} from "../index";

export class CompletedRequestDayIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestDayIntent'
            && request.dialogState === 'COMPLETED';
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const dayInput = request.intent.slots.dayOfWeek.value;
        const outputRealDay = CustomDateFormatter.getRealDate(dayInput.toLowerCase());
        if (outputRealDay === 'Samstag' || outputRealDay === 'Sonntag') {
            const text = `${dayInput} ist kein Arbeitstag im Unternehmen. Dementsprechend ist die Kantine ${dayInput} geschlossen. ` +
                `Bitte nenne mir einen Wochentag zwischen Montag und Freitag.`;
            return input.responseBuilder
                .speak(text)
                .reprompt(text)
                .addElicitSlotDirective('dayOfWeek')
                .getResponse();
        }

        let result;
        try {
            result = await database.query('SELECT * ' +
                ' FROM tagesplan WHERE tag_name = ' + '"' + outputRealDay + '"');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }

        const weekJson = JSON.parse(JSON.stringify(result));

        for (let i = 0; i < weekJson.length; i++) {
            const currentDay = weekJson[i];
            if (outputRealDay.toLowerCase() === (currentDay['tag_name'] as string).toLowerCase()) {
                return input.responseBuilder
                    .speak(`${dayInput} gibt es als Hauptgericht ${currentDay['hauptspeise']}. Als Nachspeise wird es ${currentDay['nachspeise']} geben. ` +
                        `${currentDay['tagessuppe']} wird als Tagessuppe angeboten. ` +
                        `${Messages.lastSentence[Math.floor(Math.random() * Messages.lastSentence.length)]}`)
                    .withShouldEndSession(true)
                    .getResponse();
            }
        }

        return input.responseBuilder
            .speak('Ich bitte um Entschuldigung. Leider kam es zu einem unerwarteten Fehler.')
            .withShouldEndSession(true)
            .getResponse();
    }

}

