import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class NoValueRequestDayIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestDayIntent'
            && !request.intent.slots.dayOfWeek.value
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .speak(`Für welchen Tag möchtest du eine Auskunft erhalten?`)
            .reprompt('Ich benötige die Angabe des Wochentages, für den ich dir ein Gericht raussuchen soll.')
            .addElicitSlotDirective('dayOfWeek')
            .getResponse();
    }

}