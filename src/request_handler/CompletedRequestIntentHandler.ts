import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Messages} from "../utils/Messages";
import {database} from "../index";
import {CustomDateFormatter} from "../utils/CustomDateFormatter";
import {Response} from "ask-sdk-model";

export class CompletedRequestIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestIntent'
            && request.dialogState === 'COMPLETED';
    }

    async handle(input: HandlerInput): Promise<Response> {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const dayInput = request.intent.slots.dayOfWeek.value;
        const outputRealDay = CustomDateFormatter.getRealDate(dayInput.toLowerCase());
        if (outputRealDay === 'Samstag' || outputRealDay === 'Sonntag') {
            const text = `${dayInput} ist kein Arbeitstag im Unternehmen. Dementsprechend ist die Kantine ${dayInput} geschlossen. ` +
                `Bitte nenne mir einen Wochentag zwischen Montag und Freitag.`;
            return input.responseBuilder
                .speak(text)
                .reprompt(text)
                .addElicitSlotDirective('dayOfWeek')
                .getResponse();
        }

        const menuTypeInput = request.intent.slots.menuType.resolutions.resolutionsPerAuthority[0].values[0].value.name;

        let result;
        try {
            result = await database.query('SELECT ' + menuTypeInput.toLowerCase() +
                ' FROM tagesplan WHERE tag_name = ' + '"' + outputRealDay + '"');
        } catch {
            return input.responseBuilder
                .speak('Ich bitte um Entschuldigung. Leider kam es zu einem Fehler mit der Datenbank.')
                .withShouldEndSession(true)
                .getResponse();
        }
        const menuJson = JSON.parse(JSON.stringify(result))[0];
        const menu = menuJson[Object.keys(menuJson)[0]];

        const speechText = `${dayInput} gibt es in der Kantine als ${menuTypeInput} ${menu} zu essen. ` +
            `${Messages.lastSentence[Math.floor(Math.random() * Messages.lastSentence.length)]}`;
        return input.responseBuilder
            .speak(speechText)
            .withShouldEndSession(true)
            .getResponse()
    }

}