import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class MenuTypeRequestIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestIntent'
            && request.intent.slots.menuType.value
            && !request.intent.slots.dayOfWeek.value
    }

    handle(input: HandlerInput): Response {
        const request = input.requestEnvelope.request;
        if (request.type !== 'IntentRequest') return;

        const menuTypeInput = request.intent.slots.menuType.resolutions.resolutionsPerAuthority[0].values[0].value.name;

        return input.responseBuilder
            .speak(`Für welchen Tag möchtest du eine Auskunft über die ${menuTypeInput} erhalten?`)
            .reprompt('Ich benötige die Angabe des Wochentages, für den ich dir ein Gericht raussuchen soll.')
            .addElicitSlotDirective('dayOfWeek')
            .getResponse();
    }

}