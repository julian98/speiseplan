import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response} from "ask-sdk-model";

export class StartedRequestIntentHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        const request = input.requestEnvelope.request;

        return request.type === 'IntentRequest'
            && request.intent.name === 'RequestIntent'
            && request.dialogState !== 'COMPLETED';
    }

    handle(input: HandlerInput): Response {
        return input.responseBuilder
            .addDelegateDirective()
            .getResponse();
    }

}