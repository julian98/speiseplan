import {HandlerInput, RequestHandler} from "ask-sdk-core";
import {Response, SessionEndedRequest} from "ask-sdk-model";

export class SessionEndedHandler implements RequestHandler {

    canHandle(input: HandlerInput): boolean {
        return input.requestEnvelope.request.type === "SessionEndedRequest";
    }

    handle(input: HandlerInput): Response {
        console.log(`Session ended with reason: ${(input.requestEnvelope.request as SessionEndedRequest).reason}`);

        return input.responseBuilder.getResponse();
    }

}