import {Messages} from "./Messages";

export class CustomDateFormatter {

    static getRealDate(input: string): string {
        if (!(input !== 'heute' && input !== 'morgen' && input !== 'übermorgen')) {
            const date = new Date();
            let dateString;

            if (input === 'morgen') {
                date.setDate(date.getDate() + 1);
            } else if (input === 'übermorgen') {
                date.setDate(date.getDate() + 2);
            }
            dateString = date.toLocaleString('de-DE', {
                weekday: 'long'
            }).toLowerCase();

            return Messages.englishToGermanDays[dateString];

        } else {
            return input.charAt(0).toUpperCase() + input.slice(1);
        }
    }

}