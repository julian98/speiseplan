export class Messages {

    static readonly englishToGermanDays = [];
    static readonly firstSentence = [
        'Das mache ich doch gerne!',
        'Sehr gerne.',
        'Gerne.',
        'Gern.',
        'Natürlich!',
        'Okay.',
        'Verstanden!',
        'Klar.'
    ];
    static readonly helpSentence = [
        'Wie kann ich dir weiterhelfen?',
        'Was möchtest du tun?',
        'Wobei kann ich dir helfen?',
        'Wie kann ich dir behilflich sein?'
    ];
    static readonly unknownSentence = [
        'Ich habe dich leider nicht verstanden.',
        'Ich weiß leider nicht was du meinst.',
        'Ich weiß leider nicht was du mir sagen möchtest.'
    ];

    static readonly lastSentence = [
        'Ich stehe dir weiterhin gerne zur Verfügung, bis dann!',
        'Ich hoffe ich konnte dir weiterhelfen. Auf Wiedersehen!',
        'Falls du noch etwas brauchst, kannst du dich gerne jederzeit an mich wenden.',
        'Schönen Tag noch!'
    ];

    static initialize() {
        this.englishToGermanDays['monday'] = 'Montag';
        this.englishToGermanDays['tuesday'] = 'Dienstag';
        this.englishToGermanDays['wednesday'] = 'Mittwoch';
        this.englishToGermanDays['thursday'] = 'Donnerstag';
        this.englishToGermanDays['friday'] = 'Freitag';
        this.englishToGermanDays['saturday'] = 'Samstag';
        this.englishToGermanDays['sunday'] = 'Sonntag';
    }

    static capitalize(input: string): string {
        let result = "";
        input.split(" ").forEach(value => {
            result += (value.charAt(0).toUpperCase() + value.slice(1) + " ");
        });
        return result.substr(0, result.length - 1);
    }

}
